import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Random;

public class Runner {
	public static final Random rnd = new Random();
	public static final int STEP = 1000;

	public static void main (String[] args) {
		int time = 2*1000;
		for (int i=2 ; i<=6 ; i++) {
			int len = (int)Math.pow(10,i);
			arrayList(len, time);
			hashSetInteger(len, time);
			hashSetCustom(len, time);
			linkedHashSetInteger(len, time);
		}
	}

	public static void arrayList(int len, int time) {
		System.out.print("ArrayList(" + len + "): ");
		ArrayList<Integer> list = new ArrayList<Integer>(len);
		for (int i=0 ; i<len ; i++) {
			list.add(new Integer(i));
		}
		long start = System.currentTimeMillis();
		long t = 0;
		int n = 0;
		do {
			for (int i=0 ; i<STEP ; i++) {
				list.contains(new Integer(rnd.nextInt() % (len * 2)));
			}
			n += STEP;
			t = System.currentTimeMillis() - start;
		} while (t < time);
		System.out.println((double)n / (double)t * 1000.0);
	}

	public static void hashSetInteger(int len, int time) {
		System.out.print("HashSet<Integer>(" + len + "): ");
		HashSet<Integer> list = new HashSet<Integer>(len);
		for (int i=0 ; i<len ; i++) {
			list.add(new Integer(i));
		}
		long start = System.currentTimeMillis();
		long t = 0;
		int n = 0;
		do {
			for (int i=0 ; i<STEP ; i++) {
				list.contains(new Integer(rnd.nextInt() % (len * 2)));
			}
			n += STEP;
			t = System.currentTimeMillis() - start;
		} while (t < time);
		System.out.println((double)n / (double)t * 1000.0);
	}


	private static class Custom {
		public int val;
		public Custom(int val) { this.val = val; }
		public int hashCode() { return val; }
	}
	public static void hashSetCustom(int len, int time) {
		System.out.print("HashSet<Custom>(" + len + "): ");
		HashSet<Custom> list = new HashSet<Custom>(len);
		for (int i=0 ; i<len ; i++) {
			list.add(new Custom(i));
		}
		long start = System.currentTimeMillis();
		long t = 0;
		int n = 0;
		do {
			for (int i=0 ; i<STEP ; i++) {
				list.contains(new Custom(rnd.nextInt() % (len * 2)));
			}
			n += STEP;
			t = System.currentTimeMillis() - start;
		} while (t < time);
		System.out.println((double)n / (double)t * 1000.0);
	}

	public static void linkedHashSetInteger(int len, int time) {
		System.out.print("LinkedHashSet<Integer>(" + len + "): ");
		LinkedHashSet<Integer> list = new LinkedHashSet<Integer>(len);
		for (int i=0 ; i<len ; i++) {
			list.add(new Integer(i));
		}
		long start = System.currentTimeMillis();
		long t = 0;
		int n = 0;
		do {
			for (int i=0 ; i<STEP ; i++) {
				list.contains(new Integer(rnd.nextInt() % (len * 2)));
			}
			n += STEP;
			t = System.currentTimeMillis() - start;
		} while (t < time);
		System.out.println((double)n / (double)t * 1000.0);
	}
}
